<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;

class OrderStatusUpdated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FcmChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toFcm($notifiable)
    {
        // return FcmMessage::create()
        //     ->setData([
        //         'title' => 'Order Status Updated',
        //         'body' => 'The status of your order has been updated.',
        //         'order_status' => $this->order->status,
        //     ]);
        $deviceToken = $notifiable->device_token; // Assuming you have a device_token field in your user model
        $notification = [
            'title' => 'Order Status Updated',
            'body' => 'The status of your order has been updated.',
            'order_status' => $this->order->status,
        ];

        $data = [
            'key1' => 'value1',
            'key2' => 'value2',
        ];

        $payload = [
            'to' => $deviceToken,
            'notification' => $notification,
            'data' => $data,
        ];

        return $payload;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
