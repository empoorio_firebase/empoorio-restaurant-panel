<?php

namespace App\Http\Resources\V2;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MenuMiniCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($data) {
                    

                return [
                    'id' => $data->id,
                    'name' => $data->name,
                    'unit' => $data->unit,
                    'unit_price' => $data->unit_price,
                    'added_by' => $data->added_by,
                    'published' => $data->published,
                    'user_id' => $data->user_id,
                    'attributes' => $data->attributes,
                    'addons' => $data->addons,
                    'food_specifications' => $data->food_specifications,
                    'ingredients' => $data->ingredients,
                   
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
