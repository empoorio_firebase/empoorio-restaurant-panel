<?php

namespace App\Http\Resources\V2;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\Review;
use App\Models\Attribute;


class ProductDetailCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($data) {
                    

                return [
                    'id' => $data->id,
                    'name' => $data->getTranslation('name'),
                    'unit' => $data->getTranslation('unit'),
                    'unit_price' => $data->getTranslation('unit_price'),
                    'added_by' => $data->getTranslation('added_by'),
                    'published' => $data->getTranslation('published'),
                    'user_id' => $data->getTranslation('user_id'),
                   
                ];
            })
        ];
    }
    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }

    
}
