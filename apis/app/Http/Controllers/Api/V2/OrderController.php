<?php

namespace App\Http\Controllers\Api\V2;

use App\Models\Address;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Cart;
use App\Models\Product;
use App\Models\OrderDetail;
use App\Notifications\OrderStatusUpdated;
use App\Models\Coupon;
use App\Models\CouponUsage;
use App\Models\BusinessSetting;
use App\Models\User;
use DB;
use \App\Utility\NotificationUtility;
use App\Models\CombinedOrder;
use App\Http\Controllers\AffiliateController;

class OrderController extends Controller
{
    

    public function updateStatus(Request $request, $orderId)
    {
        // Retrieve the order
        $order = Order::findOrFail($orderId);

        // Update the order status
        $order->order_status = $request->input('status');
        $order->save();

        // Send notification
        $user = $order->user; // Assuming you have a relationship between Order and User models
        $notification = new OrderStatusUpdated($order);
        $payload = $notification->toFcm($user);
        // $user->notify(new OrderStatusUpdated($order));

        // Return a response
        return response()->json([
            'message' => 'Order status updated successfully',
            'order' => $order
        ]);
    }
}
