<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Resources\V2\ClassifiedProductDetailCollection;
use App\Http\Resources\V2\ClassifiedProductMiniCollection;
use Cache;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Utility\SearchUtility;
use App\Utility\CategoryUtility;
use App\Http\Resources\V2\MenuMiniCollection;

use App\Http\Resources\V2\MenuDetailCollection;



class MenuController extends Controller
{
    public function index()
    {
        return new MenuMiniCollection(Menu::latest()->paginate(10));
    }

    public function show($id)
    {
        return new MenuDetailCollection(Menu::where('id', $id)->get());
        
    }


    public function store(Request $request){
        
        return Menu::create([
            'name' => $request->name,
            'published' => $request->published,           
            'added_by' => $request->added_by,
            'user_id' => $request->user_id,       
            'category_id' => $request->category_id,       
            'condition' => $request->condition,
            'description' => $request->description,
            'unit' => $request->unit,
            'unit_price' => $request->unit_price,
            'addons' => $request->addons,
            'food_specifications' => $request->food_specifications,
            // 'attributes' => $request->attributes,
            'ingredients' => $request->ingredients,
         
        ]);
       

    }


    public function update(Request $request, $id)
    {
        $menu = Menu::findOrFail($id);
    
        $menu->name = $request->input('name');
        $menu->published = $request->input('published');
        $menu->added_by = $request->input('added_by');
        $menu->user_id = $request->input('user_id');
        $menu->category_id = $request->input('category_id');
        $menu->condition = $request->input('condition');
        $menu->description = $request->input('description');
        $menu->unit = $request->input('unit');
        $menu->unit_price = $request->input('unit_price');
        $menu->addons = $request->input('addons');
        $menu->food_specifications = $request->input('food_specifications');
        $menu->attributes = $request->input('attributes');
        $menu->ingredients = $request->input('ingredients');
    
        $menu->save();
    
        return response()->json(['message' => 'Menu updated successfully'], 200);
    }




    // public function home()
    // {
    //     return new ProductCollection(Product::inRandomOrder()->physical()->take(50)->get());
    // }
}
