<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Resources\V2\UserCollection;
use App\Models\User;
use App\Models\WorkingHour;
use Illuminate\Http\Request;

use Laravel\Sanctum\PersonalAccessToken;


class WorkingHourController extends Controller
{
    public function index()
    {
        return WorkingHour::where('user_id', auth()->user()->id)->get();
    }

    public function update(Request $request)
    {
        $working_hour_id = WorkingHour::findOrFail($request->working_hour_id);
        $working_hour_id->update([
            'from' => $request->from,           
            'to' => $request->to,
        ]);
        return response()->json([
            'message' => translate('Profile information has been updated successfully')
        ]);
    }

    public function store(Request $request){
        return WorkingHour::create([
            'day' => $request->day,
            'from' => $request->from,           
            'to' => $request->to,
            'user_id' => $request->user_id,
            
        ]);
    }

   
}
