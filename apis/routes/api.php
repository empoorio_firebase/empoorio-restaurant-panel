<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v2/auth', 'middleware' => ['app_language']], function () {
    Route::post('login', 'App\Http\Controllers\Api\V2\AuthController@login');
   
});


Route::group(['prefix' => 'v2', 'middleware' => ['app_language']], function () {


    Route::post('profile/update', 'App\Http\Controllers\Api\V2\ProfileController@update')->middleware('auth:sanctum');
    Route::post('profile/update-image', 'App\Http\Controllers\Api\V2\ProfileController@updateImage')->middleware('auth:sanctum');
    Route::post('profile/image-upload', 'App\Http\Controllers\Api\V2\ProfileController@imageUpload')->middleware('auth:sanctum');


    //
    
    Route::get('user/info/{id}', 'App\Http\Controllers\Api\V2\UserController@info')->middleware('auth:sanctum');
    Route::post('user/info/update', 'App\Http\Controllers\Api\V2\UserController@updateName')->middleware('auth:sanctum');
    Route::post('get-user-by-access_token', 'App\Http\Controllers\Api\V2\UserController@getUserInfoByAccessToken');
    Route::apiResource('products', 'App\Http\Controllers\Api\V2\ProductController')->except(['destroy']);
    Route::apiResource('menus', 'App\Http\Controllers\Api\V2\MenuController')->except(['update', 'destroy']);
    Route::put('order/{orderId}/status', 'App\Http\Controllers\Api\V2\OrderController@updateStatus');


    Route::apiResource('withdraw-request', 'App\Http\Controllers\Api\V2\WithDrawRequestController')->except(['update', 'destroy'])->middleware('auth:sanctum');
    Route::apiResource('working-hour', 'App\Http\Controllers\Api\V2\WorkingHourController')->except(['destroy'])->middleware('auth:sanctum');



});

Route::fallback(function () {
    return response()->json([
        'data' => [],
        'success' => false,
        'status' => 404,
        'message' => 'Invalid Route'
    ]);
});
